
// imports
import React, { Component } from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';

// styles
import 'normalize.css';
import './styles/globals.css';

// components
import SplashScreen from './components/splash-screen';
import DetailsGray from './components/details-gray';
import DetailsRed from './components/details-red';
import DetailsWhite from './components/details-white';
import BankInterface from './components/bank-interface';
import BigButton from './components/big-button';
import ProdIntro from './components/prod-intro';
import EndingScreen from './components/ending-screen';
import AnimateRed from './components/animate-red';
//image
import orientation from './images/phone.png'

// Component: App
export default class App extends Component {

    render () {
        return (
            <Router>
                    <div>
                        <div id="orientation">
                            <div className="inner">
                                <h4 className="ff-frLight">Kindly rotate your device to the landscape orientation to view this product demo.</h4>
                                <img src={orientation}/>
                            </div>
                        </div>
                        { /* splash screen */ }
                        <Route exact path="/" render={props => <SplashScreen campaign="Virtual Accounts" to="/intro-b" />} />

                        { /* Intros */ }
                        <Route path="/intro-a" render={props => <DetailsGray text={["Instant Cross-Border Payments", "between DBS Accounts", "Across 6 Asian Markets"]} to="/transferBetter" />} />
                        <Route path="/intro-b" render={props => <DetailsGray text={["Standard", "Telegraphic Transfers"]} to="/transfer" cta />} />

                        { /* Bank UI */ }
                        <Route path="/transfer" render={props => <BankInterface to="/detail-a" copy={["Beneficiary Receives Funds", "After 1 Week"]} speed="slow" />} />

                        { /* Results */ }
                        <Route path="/detail-a" render={props => <DetailsRed head={["Long Wait For Payment"]} to="/detail-b" />} />
                        <Route path="/detail-b" render={props => <DetailsRed head={["Sub-Optimal", "Cash Management"]} to="/detail-c" />} />
                        <Route path="/detail-c" render={props => <DetailsRed head={["Impacted Supplier", "Relationship"]} to="/solution" />} />

                        { /* Big button */ }
                        <Route path="/solution" render={props => <BigButton textSmall="Find Out About" textLarge="A Better Solution" to="/prod-intro" />} />

                        { /* Product Introduction */ }
                        <Route path="/prod-intro" render={props => <ProdIntro prodName="PriorityPay" to="/intro-a" />} />

                        { /* Bank UI 2 */ }
                        <Route path="/transferBetter" render={props => <BankInterface to="/details-d" copy={["Beneficiary Receives Funds", "Instantly"]} speed="fast" />} />

                        {/* what this means */}
                        <Route path="/details-d" render={props => <DetailsGray text={["What This Means For You"]} to="/details-f" cta />} />

                        {/* Outros */}
                        <Route path="/details-f" render={props =>
                                <AnimateRed animateText={[
                                        ["Smoother Business", "Operations"],
                                        ["Better Visibility with", "Instant Credit Confirmation"],
                                        ["Better Relationships", "with Suppliers"]
                                    ]} to="/ending-a" />
                            }/>

                        { /* Ending */ }
                        <Route path="/ending-a" render={props => <DetailsWhite text={["Instant Cross-Border Payments", "between DBS Accounts", "Across 6 Asian Markets"]}  to="/ending-b" />} />
                        <Route path="/ending-b" render={props => <DetailsWhite animate={["PriorityPay", "Dont Wait Any Longer"]}  to="/thend" />} />
                        <Route path="/thend" render={props => <EndingScreen home="/" replay="/" />} />
                </div>
            </Router>
        )
    }
}
