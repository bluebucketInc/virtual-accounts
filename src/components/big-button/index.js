
// imports
import React from 'react';
import { Link } from 'react-router-dom';

import './styles.css';


// Component: slash screen [stateless]
const BigButton = (props) => {

    // render
    return (
        <div className="text-center container container-m valign-center">
            <Link to={props.to}>
                <button className="btn btn-red btn-xl"><span>{props.textSmall}</span><br/>{props.textLarge}</button>
            </Link>
        </div>
    )
};

// export
export default BigButton;
