
// imports
import React from 'react';
import { withRouter } from 'react-router-dom';
import { TimelineMax } from 'gsap';

import './styles.css';


// Component: slash screen [stateless]
const DetailsWhite = (props) => {

    // timeline
    this.timeline = new TimelineMax();

    // content
    const text = props.text || props.animate;
    const headers = text.map( (text, i) => <h2 className="ff-frBold fc-white" key={text.toString()} id={`header${i}`}>{text}</h2> );

    // render function
    const render = () => {

         // callback    
        this.timeline.eventCallback("onComplete", () => setTimeout(() => props.history.push(props.to), 1000));

        // start timeline
        this.timeline

            // 1. 0 -> smaller, 1 -> goes up
            .to('#header0', 1, {
                opacity: 0.2,
                scale : 0.7,
                y: '-=105'
            })
            // 2. 0 -> smaller, 1 -> goes up
            .to('#header1', 0.8, {
                scale : 1,
                y: '-=115'
            }, '-=0.3')
    }

    // animate
    if (props.animate) {

        setTimeout(() => render(), 1000);

    }

    // redirect if no CTA
    props.cta || props.animate || setTimeout(() => props.history.push(props.to), props.timeout || 2000);

    // render
    return (
        <div className="container bg-black bg-black-container details-white">
            <div className="text-center valign-center">
                <div className="multiliner">
                    { props.animate
                        ? <div className="animate-container">{headers}</div>
                        : headers
                    }
                </div>
            </div>
        </div>
    )
};

// export
export default withRouter(DetailsWhite);
