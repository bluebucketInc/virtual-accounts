
// imports
import React from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';

import './styles.css';


// Component: slash screen [stateless]
const DetailsRed = (props) => {

    // content - head
    const text = props.head;
    const headers = text.map( text => <h2 className="ff-frBold fc-gradient-red" key={text.toString()}>{text}</h2> );

    // redirect if no CTA
    props.cta || setTimeout(() => props.history.push(props.to), props.timeout || 2000);

    // render
    return (
        <div className="text-center container container-m valign-center details-red">

            { /* Main */ }
            <div className="multiliner main-head">{headers}</div>

            {/* CTA */}
            { props.cta &&
                <Link to={props.to}>
                    <button className="btn btn-red btn-m">Next</button>
                </Link>
            }
        </div>
    )
};

// export
export default withRouter(DetailsRed);
