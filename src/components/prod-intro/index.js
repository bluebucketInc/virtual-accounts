
// imports
import React from 'react';
import { Link } from 'react-router-dom';

import './styles.css';


// Component: slash screen [stateless]
const ProdIntro = (props) => {

    // render
    return (
        <div className="valign-center container container-s ">
            <div className="text-left prod-intro">
                <h3 className="ff-arial fc-gray">Introducing</h3>
                <h2 className="ff-frBold fc-gradient-gray">{props.prodName}</h2>
                <Link to={props.to}>
                    <button className="btn btn-red btn-m">Next</button>
                </Link>
            </div>
        </div>
    )
};

// export
export default ProdIntro;
