
// imports
import React from 'react';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { TimelineMax } from 'gsap';

import './styles.css';


// Component: slash screen [stateless]
const AnimateRed = (props) => {

    // ease
    this.timeline = new TimelineMax();
    // timeline
    this.time = 1;

    // content
    const text = props.animateText;
    const content = text.map( (text, index) => {
        return (
            <div className="animate-container" key={text.toString()} id={`animCtr${index}`}>
                { text.map( text => <h2 className="ff-frBold fc-gradient-red" key={text.toString()}>{text}</h2>) }
            </div>
        )
    });

    // animation
    const render = () => {

        // start timeline
        this.timeline

            // 1. 0 -> smaller, 1 -> goes up
            .to('#animCtr0', 1, {
                filter: 'grayscale(1)',
                scale : 0.6,
                y: '-=190'
            })
            .to('#animCtr1', 0.7, {
                y: '-=200'
            }, '-=0.5')

            // 2. 0 -> disappear, 1 -> smaller and goes up
            .to('#animCtr0', 0.5, {
                y: '-=100',
                scale: 0.5,
                opacity: 0
            }, '+=2')
            .to('#animCtr1', 1, {
                filter: 'grayscale(1)',
                scale : 0.6,
                y: '-=250'
            })

            // 3. 2 -> goes up
            .to('#animCtr2', 0.7, {
                y: '-=450'
            }, '-=0.5')

            // show button
            .to('#nextBtn', 0.1, {
                opacity: 1
            }, '+=1');
    };

    // call render
    setTimeout( () => render(), 500);

    // render
    return (
        <div className="text-center container container-m valign-center details-red">

            { /* Main */ }
            <div className="multiliner main-head animation-wrapper">{content}</div>

            {/* CTA */}
            <Link to={props.to}>
                <button id="nextBtn" style={{opacity:0}} className="btn btn-red btn-m">Next</button>
            </Link>
        </div>
    )
};

// export
export default withRouter(AnimateRed);
