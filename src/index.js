
// react
import React from 'react';
import ReactDOM from 'react-dom';

import App from './app.js';

// service worker
import registerServiceWorker from './sw';

// render
ReactDOM.render(<App />, document.getElementById('root'));

// register sw
registerServiceWorker();
